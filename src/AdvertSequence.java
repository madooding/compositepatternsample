import java.util.ArrayList;
import java.util.List;

public class AdvertSequence extends MediaClip {
	private List<MediaClip> mediaList = new ArrayList<>();
	
	public void addClip(MediaClip mc){
		this.mediaList.add(mc);
	}
	
	public void removeClip(int index){
		this.mediaList.remove(index);
	}
	
	public void removeClip(MediaClip mc){
		this.mediaList.remove(mc);
	}
	
	public MediaClip getChild(int index){
		return this.mediaList.get(index);
	}
	
	@Override
	public void play() {
		for(MediaClip mc : mediaList){
			mc.play();
		}
	}

}

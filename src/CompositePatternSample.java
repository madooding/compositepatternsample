
public class CompositePatternSample {
	public static void main(String[] args){
		AdvertSequence advertSequence = new AdvertSequence();
		advertSequence.addClip(new VideoClip());
		advertSequence.addClip(new AudioClip());
		advertSequence.addClip(new VideoClip());
		advertSequence.addClip(new VideoClip());
		advertSequence.addClip(new AudioClip());
		advertSequence.addClip(new VideoClip());
		advertSequence.addClip(new AudioClip());
		advertSequence.play();
	}
}
